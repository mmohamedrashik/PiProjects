/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspberry;


import java.net.*;
import java.io.*;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
/**
 *
 * @author Mohammed Rashik
 */
public class RaspLEDServer {
     ServerSocket ss;
     Socket s;
     GpioController gpio;
     GpioPinDigitalOutput pin,pin2;
    RaspLEDServer() 
    {
     gpio = GpioFactory.getInstance(); // CREATING Controller
     pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "GREEN", PinState.LOW);
     pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "red", PinState.LOW);
     pin.setShutdownOptions(true, PinState.LOW);
     pin2.setShutdownOptions(true, PinState.LOW);
        
    }
    public void server_start()
    {

        System.out.println("SERVER RUNNING");
        try{
        ss=new ServerSocket(7777);  
        s=ss.accept();  
        DataInputStream din=new DataInputStream(s.getInputStream()); 
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in)); 
        String str="",str2="";  
        while(!str.equals("stop")){  
        str=din.readUTF();  
         
        if(str.equals("GREEN"))
        {
            pin.toggle();
            if(pin.getState() == PinState.HIGH)
            {
                System.out.println("GREEN LIGHT IS ON");
            }else
            {
                System.out.println("GREEN LIGHT IS OFF");
            }
        }else if(str.equals("RED"))
        {
            pin2.toggle();
            if(pin2.getState() == PinState.HIGH)
            {
                System.out.println("RED LIGHT IS ON");
            }else
            {
                System.out.println("RED LIGHT IS OFF");
            }
        }
        }  
        din.close();  
        s.close();  
        ss.close(); 
        }catch(Exception e)
        {
            e.printStackTrace();
        }
       

    }
}
