/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author Mohammed Rashik
 */
public class RelaySwitch {
    RelaySwitch() throws Exception
    {
        System.out.println("STARTING GPIO");
        final GpioController gpio = GpioFactory.getInstance(); // CREATING Controller
        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "GREEN", PinState.HIGH);

        pin.setShutdownOptions(true, PinState.LOW);
          pin.high();
        Thread.sleep(5000);
   //  gpio.shutdown();
     System.out.println("ENDING GPIO");

    }
}
