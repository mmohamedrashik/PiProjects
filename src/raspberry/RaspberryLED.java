/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author Mohammed Rashik
 */
public class RaspberryLED {
    
    RaspberryLED() throws InterruptedException
    {
        System.out.println("STARTING GPIO");
        final GpioController gpio = GpioFactory.getInstance(); // CREATING Controller
        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "GREEN", PinState.HIGH);
        final GpioPinDigitalOutput pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "red", PinState.LOW);
        pin.setShutdownOptions(true, PinState.LOW);
        pin2.setShutdownOptions(true, PinState.LOW);
        
     
       
        for(int i=0;i<10;i++)
        {
        pin.toggle();
        pin2.toggle();
        Thread.sleep(2000);
        }
        gpio.shutdown();
     System.out.println("ENDING GPIO");
    }
}
